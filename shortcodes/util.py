import string
import random
from datetime import date
from .models import Statistics


def generate_shortcode():
    return ''.join(random.choices(string.ascii_letters + string.digits + '_', k=6))


def get_response_headers(self, data):
    return {'Location': data}


def update_statistics(shortcode):
    today = date.today()
    statistics_data = {
        'shortcode': shortcode,
        'year': today.year,
        'week': today.isocalendar()[1]
    }
    statistics_data = Statistics.objects.get_or_create(**statistics_data)[0]
    statistics_data.counter += 1
    statistics_data.save()

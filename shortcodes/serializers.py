from rest_framework import serializers
from .models import Shortcode, Statistics


class ShortcodeSerializer(serializers.HyperlinkedModelSerializer):
    shortcode = serializers.CharField(required=False)

    class Meta:
        model = Shortcode
        fields = ('shortcode', 'url')


class StatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Statistics
        fields = ('shortcode', 'year', 'week', 'counter')

from django.db import models


class Url(models.Model):
    url = models.CharField(max_length=100)

    def __str__(self):
        return self.url


class Shortcode(models.Model):
    shortcode = models.CharField(max_length=6, primary_key=True)
    url = models.CharField(max_length=100)

    def __str__(self):
        return self.shortcode


class Statistics(models.Model):
    shortcode = models.CharField(max_length=6)
    year = models.SmallIntegerField(default=2021)
    week = models.SmallIntegerField(default=1)
    counter = models.IntegerField(default=0)

from django.contrib import admin
from .models import Url, Shortcode, Statistics


# Register your models here.
admin.site.register(Url)
admin.site.register(Shortcode)
admin.site.register(Statistics)

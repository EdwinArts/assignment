import logging

from rest_framework import viewsets
from django.forms import modelform_factory
from .serializers import ShortcodeSerializer, StatisticsSerializer
from .models import Shortcode, Url, Statistics
from rest_framework import status
from rest_framework.response import Response
from .util import *

logger = logging.getLogger(__name__)
ShortcodeForm = modelform_factory(Shortcode, exclude=[])


class ShortcodeViewSet(viewsets.ModelViewSet):
    queryset = Shortcode.objects.all()
    serializer_class = ShortcodeSerializer

    def create(self, request, *args, **kwargs):
        logger.error(request.data)
        logger.error(generate_shortcode())
        # Check if url exists in database, else return HTTP400
        if not Url.objects.filter(url=request.data['url']).exists():
            return Response("Url not present (required)", status=status.HTTP_400_BAD_REQUEST)

        # Check if shortcode is provided, else create a shortcode
        if request.data['shortcode'] == '':
            generated_shortcode = generate_shortcode()
            # Check if generated shortcode already exists, if so, create a new shortcode
            while Shortcode.objects.filter(shortcode=generated_shortcode).exists():
                generated_shortcode = generate_shortcode()
            # Store shortcode in database
            serializer = self.get_serializer(data={'shortcode': generated_shortcode, 'url': request.data['url']})
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)

            # Set headers & return response HTTP201
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        # Check if provided shortcode exists, if so return HTTP409
        if Shortcode.objects.filter(shortcode=request.data['shortcode']).exists():
            return Response("Shortcode already in use", status=status.HTTP_409_CONFLICT)

        # Else default behavior
        return super().create(request)

    def retrieve(self, request, *args, **kwargs):
        # Retrieve the url for this shortcode and set correct headers
        instance = self.get_object()
        serialized_data = self.get_serializer(instance)
        headers = get_response_headers(self, serialized_data.data['url'])

        # Store use of shortcode in statistics database
        update_statistics(serialized_data.data['shortcode'])

        # Return http response HTTP302
        return Response(data=serialized_data.data, status=status.HTTP_302_FOUND, headers=headers)


class StatisticsViewSet(viewsets.ModelViewSet):
    queryset = Statistics.objects.all()
    serializer_class = StatisticsSerializer

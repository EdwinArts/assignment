from django.test import TestCase
from .models import Url, Shortcode, Statistics
from .util import update_statistics, generate_shortcode

import logging

logger = logging.getLogger(__name__)

class ModelTestCase(TestCase):
    def setUp(self):
        Url.objects.create(url='https://google.nl')
        Shortcode.objects.create(shortcode='lskdjf', url='https://google.nl')
        Statistics.objects.create(shortcode='lskdjf', year=2021, week=30, counter=5)
        Statistics.objects.create(shortcode='lskdjf', year=2021, week=31, counter=15)
        Statistics.objects.create(shortcode='lskdjf', year=2021, week=32, counter=51)
        Statistics.objects.create(shortcode='aaaaaa', year=2021, week=32, counter=51)

    def test_url_case(self):
        url = Url.objects.get(url='https://google.nl')
        self.assertEqual(url.url, 'https://google.nl')

    def test_shortcode_case(self):
        shortcode = Shortcode.objects.get(shortcode='lskdjf')
        self.assertEqual(shortcode.shortcode, 'lskdjf')
        self.assertEqual(shortcode.url, 'https://google.nl')

    def test_statistics_case(self):
        statistics = Statistics.objects.all()
        self.assertEqual(statistics.filter(shortcode='lskdjf').count(), 3)


class StatisticsTestCase(TestCase):
    def setUp(self):
        Url.objects.create(url='https://google.nl')
        Shortcode.objects.create(shortcode='lskdjf', url='https://google.nl')

    def test_update_statistics(self):
        update_statistics('lskdjf')
        queryset = Statistics.objects.all()
        self.assertEqual(queryset.count(), 1)
        self.assertEqual(queryset.get(shortcode='lskdjf').counter, 1)

    def test_generate_shortcode(self):
        shortcode1 = generate_shortcode()
        shortcode2 = generate_shortcode()
        self.assertNotEqual(shortcode1, shortcode2)
        for x in range(100):
            logger.error(generate_shortcode())

